package com.kshrd.controller;
import com.kshrd.model.DataTable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class HomeController {
    @GetMapping("/home")
    public String homePage(Model model){
        model.addAttribute("dataTable", Arrays.asList(
                new DataTable(1,"Aliens","Description","image/Aliens_1.jpg"),
                new DataTable(2,"Aliens","Description","image/Aliens_1.jpg"),
                new DataTable(3,"Aliens","Description","image/Aliens_1.jpg")
        ));
        return "/home";
    }

    @GetMapping("/create")
    public String createPage(){
        return "/createActicle";
    }

    @GetMapping("/view")
    public String viewPage(){
        return "/viewActicle";
    }

    @GetMapping("edit")
    public String editPage(){
        return "/editActicle";
    }
}
